import yaml
import threading
import requests
from custom_ios import CustomIOSDriver
from netmiko.exceptions import ReadTimeout
import os
from dotenv import load_dotenv

load_dotenv()
api_url = os.environ["API_URL"]

with open('variable.yaml', 'r') as f:
    devices = yaml.safe_load(f)


def run_device_check():
    for device in devices:
        try:
            optional_args = device.get('optional_args', {})
            device_connection = CustomIOSDriver(device['ip_address'], device['username'], device['password'],
                                                optional_args=optional_args)
            device_connection.open()

            facts = device_connection.get_facts()
            interfaces = device_connection.get_interfaces()
            environment = device_connection.get_environment()

            device_connection.close()

            cpu_usage = environment.get('cpu', {}).get('0', {}).get('%usage', 0)
            used_memory = environment.get('memory', {}).get('used_ram', 0)

            print("Device: {}".format(device['ip_address']))
            print("Hostname: {}".format(facts['hostname']))
            print("Uptime: {}".format(facts['uptime']))
            print(environment)
            print("CPU Usage: {}%".format(cpu_usage))
            print("Used Memory: {} KB".format(used_memory))

            for interface in interfaces:
                print("Interface: {}".format(interface))
                print("Operational status: {}".format(interfaces[interface]['is_up']))
            data = {
                "ipAddress": device['ip_address'],
                "port": device['optional_args']['port'],
                "transport": device['optional_args']['transport'],
                "memoryValue": used_memory,
                "cpuValue": cpu_usage,
                "netInterfaces": []
            }
            for interface in interfaces:
                data["netInterfaces"].append({
                    "nameInterface": interface,
                    "interfaceState": interfaces[interface]['is_up']
                })
            response = requests.post(api_url, json=data)

            if response.status_code == 202:
                print(f"Data was successfully sent to the REST API for the equipment {device['ip_address']}.")
            else:
                print(f"Error sending data to REST API for service {device['ip_address']}: {response.text}")
        except ReadTimeout as e:
            print(f"Error: Failed to get data for device {device['ip_address']} due to ReadTimeout error.")
            print(str(e))

    threading.Timer(60, run_device_check).start()


run_device_check()
